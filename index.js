let utils = {
	/**
	 * 根据百分比获取宽度
	 * 
	 * @param {*} percent 百分比
	 * @return 宽度
	 */
	getWidth(percent) {
		return Math.round(device.width * (percent / 100));
	},

	/**
	 * 根据百分比获取高度
	 * 
	 * @param {*} percent 百分比
	 * @return 高度
	 */
	getHeight(percent) {
		return Math.round(device.height * (percent / 100));
	},

	/**
	 * 进行点击
	 * 
	 * @param {*} widthPrecent 宽度百分比
	 * @param {*} heightPrecent 高度百分比
	 */
	onClick(widthPrecent, heightPercent) {
		// 获取高度和宽度像素
		let width = this.getWidth(widthPrecent);
		let height = this.getHeight(heightPercent);
		click(width, height);
	},

	/**
	 * 取色
	 * 
	 * @param {*} widthPrecent 宽度百分比
	 * @param {*} heightPrecent 高度百分比
	 */
	getColor(widthPrecent, heightPrecent) {
		// 获取高度和宽度像素
		let width = this.getWidth(widthPrecent);
		let height = this.getHeight(heightPrecent);

		// 进行截图取色
		let img = captureScreen();
		let color = images.pixel(img, width, height);
		return colors.toString(color);
	},

	/**
	 * 是否是结束页面
	 * 
	 * @param img 截图
	 */
	hasEnd(img) {
		// 宽度和高度百分比
		let widthPercent = 90;
		let heightPercent = 90;

		let colorHex = colors.toString(images.pixel(img, this.getWidth(widthPercent), this.getHeight(heightPercent)));
		return colorHex === '#fff5f5f5';
	},

	/**
	 * 是否有逛一逛
	 * 
	 * @param img 截图
	 */
	hasWalk(img) {
		// 宽度和高度百分比
		let widthPercent = 90;
		let heightPercent = 68;

		// 获取高度和宽度像素
		let widths = [];
		let heights = [];

		// 初始化高度
		for (let i = heightPercent - 15; i < heightPercent + 15; i += 0.3) {
			heights.push(this.getHeight(i));
		}

		// 初始化宽度
		for (let i = widthPercent - 15; i < widthPercent + 15; i += 0.5) {
			if (this.getWidth(i) < device.width) {
				widths.push(this.getWidth(i));
			}
		}

		for (let height of heights) {
			for (let width of widths) {
				// 取出图片中指定位置的颜色
				let colorHex = colors.toString(images.pixel(img, width, height));
				// 如果取到逛一逛的颜色
				let r = colorHex.substr(3, 1) === 'f';
				let g = colorHex.substr(5, 1) === 'a' || colorHex.substr(5, 1) === 'b';
				let b = colorHex.substr(7, 1) === '0' || colorHex.substr(7, 1) === '1' || colorHex.substr(7, 1) === '2';
				if (r && g && b) {
					return {
						width: width,
						height: height
					};
				}
			}
		}
		return null;
	}
};

let action = {
	/**
	 * 初始化
	 */
	init() {
		// 请求截图
		if (!requestScreenCapture()) {
			toast("请求截图失败");
			exit();
		}
	},

	/**
	 * 逛一逛
	 */
	walk() {
		click(utils.getWidth(80), utils.getHeight(68));
		sleep(2000);
	},

	/**
	 * 进行偷能量
	 */
	steal() {
		// 要点击的坐标
		let coordinates = [{
			x: 21,
			y: 30
		}, {
			x: 23,
			y: 29
		}, {
			x: 33.3,
			y: 26
		}, {
			x: 37,
			y: 24
		}, {
			x: 45,
			y: 23
		}, {
			x: 50,
			y: 22
		}, {
			x: 66,
			y: 26
		}, {
			x: 72,
			y: 28
		}, {
			x: 80,
			y: 29
		}]

		for (let i = 0; i < 3; i++) {
			// 最后一次从中间开始偷能量
			if (i === 2) {
				coordinates = this.random(coordinates);
				console.log('排序后的坐标', coordinates);
			}
			// 进行点击
			for (let coordinate of coordinates) {
				utils.onClick(coordinate.x, coordinate.y);
			}
			coordinates = coordinates.reverse();

			// 如果弹出装扮，截图，判断是否是帮好友复活能量
			let img  = captureScreen();
			let hex = colors.toString(images.pixel(img, utils.getWidth(50), utils.getHeight(50)));

			// 如果是帮好友复活能量，就点上面一点
			if (hex === '#ffffffff') {
				utils.onClick(50, 73);
				utils.onClick(50, 78);
			}
			
			// 如果是装扮，就点下面一点
			else {
				utils.onClick(50, 80);
				utils.onClick(50, 83);
			}
			sleep(1000);
		}
	},

	/**
	 * 排序坐标（从中间开始偷能量）
	 * 
	 * @param coordinates 坐标
	 * @return 排序后的坐标
	 */
	random(coordinates) {
		// 从数组中间开始切割
		let middle = Math.floor(coordinates.length / 2);
		// 数组的左半部分
		let lefts = coordinates.slice(0, middle + 1);
		// 数组的右半部分
		let rights = coordinates.slice(middle + 1, coordinates.length);
		let isLeft = true;
		let newArr = [];
		// 左边一个，右边一个，取到新数组
		while (lefts.length !== 0 || rights.length !== 0) {
			// 取出左半部分（从又往左取）
			if (isLeft && lefts.length !== 0) {
				newArr.push(lefts.pop());
			}
			// 取出又半部分（从左往右取）
			if (!isLeft && rights.length !== 0) {
				newArr.push(rights.shift());
			}
			isLeft = !isLeft;
		}
		return newArr;
	}
};

// 初始化
action.init();

// 打开支付宝
launchApp(getAppName("com.eg.android.AlipayGphone"));
sleep(2000);

// 偷能量
while (true) {
	// 进行截图
	let img = captureScreen();

	// 判断是否是鸟叫页面
	if (utils.hasEnd(img)) {
		alert('偷能量结束');
		exit();
	}
	
	// 获取逛一逛坐标
	let coordinate = utils.hasWalk(img);

	// 判断是否有逛一逛图标
	if (coordinate !== null) {
		// 偷能量
		action.steal();
		// 点击逛一逛
		click(coordinate.width, coordinate.height);
		sleep(2000);
	} else {
		alert('获取逛一逛坐标失败');
		exit();
	}
}